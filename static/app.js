var container;
var camera, controls, scene, renderer;
var group = new THREE.Group();
var sn_group = new THREE.Group();
var curve_group = new THREE.Group(); 
var current_surface;
var npts = 20;
var curves = [];
var surfaces = [];
var surface_normales = [];

init();
animate();

/***************************************************/
// Main app logic

function sendCode()
{
    var dataToSend = new Object();
    dataToSend.npts = npts;
    dataToSend.data = document.getElementById("code_input").value;
    display_status("Sending...", false);
    $.ajax({
        url: "/send/",
        data: JSON.stringify(dataToSend),
        type: 'POST',
        contentType: 'application/json',
        success: function(result)
        {
            document.getElementById("code_input").value = "";
            if(result == undefined) display_status("No result from server", true);
            else react_to_new_data(result);
        },
        error: function() { display_status("Connection failed", true);}
    });
}

// React to inbound data: check the status, show the messages and either update the scene or show error message and do nothing
function react_to_new_data(data)
{
    var data_lines = data.split("\n"); // first line is a status message. If it is empty, then all is ok
    if(data_lines[0] == "")
    {
        display_status("Succesfully evaluated", false);
        show_selectors();
        update_scene(data_lines);
    }
    else display_status("Error: " + data_lines[0], true);
}

// clear scene and load new geometry with default settings
function update_scene(data)
{
    drop_to_default();
    scene.remove(group); //clear scene before we replace geometry
    group = new THREE.Group();
    update_geometry(data);
    update_selectors();
    toggle_all_splines();
    switch_surface();
    toggle_sn();
    scene.add(group);
}

function drop_to_default()
{
    // reset page controls
    $('#display_all_splines').prop("checked", true);
    $('#display_surface_normales').prop("checked", false);
}


// parse inbound data and update geometry containers
function update_geometry(data)
{
    curves = [];
    surfaces = [];
    surface_normales = [];
    var n_line = data[1].split(" ");
    var n_splines = n_line[0].toString();
    var n_surfaces = n_line[1].toString();
    var i = 1;
    for(var ci = 0; ci < n_splines; ci++)
    {
        var new_geom = new THREE.Geometry();
        var offset = ci*npts + 2;
        for(var pti = 0; pti < npts; pti++)
        {
            var pts_line = data[offset + pti].split(" ");
            new_geom.vertices.push(new THREE.Vector3(
                parseFloat(pts_line[0]),
                parseFloat(pts_line[1]),
                parseFloat(pts_line[2])));
        }
        var new_curve = new THREE.Line(new_geom, new THREE.LineBasicMaterial( { linewidth: 10, color: 0xFF0000, transparent: true } ));
        curves.push(new_curve);
    }
    var l = npts*npts;
    for(var si = 0; si < n_surfaces; si++)
    {
        var new_geom = new THREE.Geometry();
        var offset = 2+ n_splines*npts + si*l;
        var vertices = [];
        for(var pti = 0; pti < l; pti++)
        {
            var pts_line = data[offset + pti].split(" ");
            vertices.push(new THREE.Vector3(
                parseFloat(pts_line[0]),
                parseFloat(pts_line[1]),
                parseFloat(pts_line[2])));
            surface_normales.push( new THREE.ArrowHelper(
                new THREE.Vector3(
                    parseFloat(pts_line[3]),
                    parseFloat(pts_line[4]),
                    parseFloat(pts_line[5])),
                new THREE.Vector3(
                    parseFloat(pts_line[0]),
                    parseFloat(pts_line[1]),
                    parseFloat(pts_line[2])),
                10, 0x3333FF));
        }
        var geometry = triangulate_mesh(vertices);
        var new_mesh = THREE.SceneUtils.createMultiMaterialObject( geometry, [
				new THREE.MeshLambertMaterial({ side: THREE.DoubleSide}),
				new THREE.MeshBasicMaterial({
					color: 0x000000,
					opacity: 0.3,
					wireframe: true,
					transparent: true})]);
        //var new_mesh = new THREE.Mesh(geometry,
          //  new THREE.MeshLambertMaterial( { color: 0xffffff, shading: THREE.FlatShading, vertexColors: THREE.VertexColors } ));
//        var new_mesh = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( { side: THREE.DoubleSide } ));
//        var new_mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial( { color: 0xfefefe, wireframe: true, opacity: 0.5 }));
        surfaces.push(new_mesh);
    }
}

function triangulate_mesh(vertices)
{
    var geometry = new THREE.Geometry();
    geometry.vertices = vertices;
    for(var row = 0; row < npts - 1; row++)
    {
        offset = row*npts;
        for(var col = 0; col < npts - 1; col++)
        {
            geometry.faces.push(new THREE.Face3(offset + col, offset + col+npts, offset +col+1));
            geometry.faces.push(new THREE.Face3(offset + col+1, offset + col+npts, offset + col+npts+1));
        }
    }
    geometry.computeFaceNormals();
    geometry.computeVertexNormals();
    return geometry;
}

// update the html code of curve and surface selector
function update_selectors()
{
    var c_selector = document.getElementById("spline_selector");
    var s_selector = document.getElementById("surf_selector");
    c_selector.innerHTML = "";
    s_selector.innerHTML = "";
    var cn = curves.length;
    for(var i = 0; i < cn; i++)
    {
        var option = document.createElement("option");
        option.text = "Spline " + (i+1).toString();
        c_selector.appendChild(option);
    }
    var sn = surfaces.length;
    for(var i = 0; i < sn; i++)
    {
        var option = document.createElement("option");
        option.text = "Surface " + (i+1).toString();
        s_selector.appendChild(option);
    }
    if(sn > 0)
    {
        $(s_selector).prop("disabled", false);
        $('#display_surface_normales').prop("disabled", false);
        s_selector.selectedIndex = 0;
    }
    else
    {
        $(s_selector).prop("disabled", true);
        $('#display_surface_normales').prop("disabled", true)
        s_selector.selectedIndex = -1;
    }
    $('#splines_count').html(cn.toString());
    $('#surfaces_count').html(sn.toString());
}

// toggle surface normales display
function toggle_sn()
{
    var el = $('#display_surface_normales');
    if(el.prop("checked")) group.add(sn_group);
    else group.remove(sn_group);
}

// Switch displayable surface
function switch_surface()
{
    if(current_surface != undefined) 
    {
        group.remove(current_surface);
        group.remove(sn_group);
    }
    sn_group = new THREE.Group(); // clear normales
    var ci = parseInt(document.getElementById("surf_selector").selectedIndex);
    if(ci >= 0) 
    {
        current_surface = surfaces[ci];
        group.add(current_surface);
        var offset = ci*npts*npts;
        var l = npts*npts;
        for(var i = 0; i < l; i++)
        {
            sn_group.add(surface_normales[offset + i]);
        }
        toggle_sn();
    }
}

function toggle_all_splines()
{
    var el = $('#display_all_splines');
    var selector = $('#spline_selector');
    if(curves.length > 0)
    {
        if(el.prop("checked"))
        {
            selector.prop("disabled", true);
        }
        else
        {
            selector.prop("disabled", false);
        }
    }
    else
    {
        selector.prop("disabled", true);
    }
    switch_spline();
}

// Switch displayable spline
function switch_spline()
{
    group.remove(curve_group);
    var el = $('#display_all_splines');
    if(el.prop("checked"))
    {
        curve_group = new THREE.Group();
        var l = curves.length;
        for(var i = 0; i < l; i++) curve_group.add(curves[i]);
        group.add(curve_group);
    }
    else
    {
        var si = parseInt(document.getElementById("spline_selector").selectedIndex);
        if(si >= 0)
        {
            curve_group = new THREE.Group();
            curve_group.add(curves[si]);
            group.add(curve_group);
        }
    }
}

function display_status(message, isError)
{
    var element = document.getElementById("smesg");
    $(element).stop(true, true);
	element.innerHTML = message;
	element.style.display = "inline";
    if(isError) element.style.color = "red";
    else element.style.color = "green";
	$(element).delay(2000).fadeOut(500);
}

/***************************************************/
// WebGL init and main loop	
function init()
{
    container = document.getElementById("render_window");
    renderer = new THREE.WebGLRenderer({antialias: true});
    //renderer.setClearColor(0xf0f0f0);
    renderer.setSize(container.clientWidth, container.clientWidth);
    container.appendChild(renderer.domElement);
    camera = new THREE.PerspectiveCamera(70, container.clientWidth/container.clientWidth, 1, 1000);
    camera.position.z = 300;
    scene = new THREE.Scene();
    scene.add(camera);
    var light = new THREE.AmbientLight( 0xf5f5dc );
    //var light = new THREE.PointLight(0xffffff, 1.5);
    //light.position.set(1000, 1000, 2000);
    scene.add(light);
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    var material = new THREE.MeshLambertMaterial( { side: THREE.DoubleSide } );
    textGeo = new THREE.TextGeometry( "Placeholder", {
        size: 40,
        height: 20,
        curveSegments: 4,
        font: "helvetiker",
        weight: "normal",
        style: "normal",
        bevelThickness: 2,
        bevelSize: 1.5,
        bevelEnabled: true,
        material: 0,
        extrudeMaterial: 1
    });
    textGeo.computeBoundingBox();
    var centerOffset = -0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
    var textmesh = new THREE.Mesh(textGeo, material);
    textmesh.position.x = centerOffset;
    group.add(textmesh);
    scene.add( group );
}

function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}

/***************************************************/
// Miscellaneous page controls

function loadExample1()
{
    document.getElementById("code_input").value = document.getElementById("e1").innerHTML;
}

function clearInput()
{
    document.getElementById("code_input").value = "";
}

function show_selectors()
{
    $("#selector_block").fadeIn().css("display","inline-block");
}

function showHelp()
{
    $('#main_screen').fadeOut(200);
    $('#help_screen').delay(200).fadeIn(200);
}

function closeHelp()
{
    $('#help_screen').fadeOut(200);
    $('#main_screen').delay(200).fadeIn(200);
}