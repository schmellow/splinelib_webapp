import ctypes
from ctypes import cdll
from ctypes import c_double
lib = cdll.LoadLibrary('./libsplinelib.so')
lib.Model_QSplineNumber.restype = ctypes.c_int
lib.Model_QSurfaceNumber.restype = ctypes.c_int
lib.Model_QStatus.restype = ctypes.c_char_p

class Model(object):
    surfaces = 0
    splines = 0
    status = ""
    def __init__(self, data_string):
        self.obj = lib.Model_new(data_string)
        self.status = lib.Model_QStatus(self.obj)
        self.splines = lib.Model_QSplineNumber(self.obj)
        self.surfaces = lib.Model_QSurfaceNumber(self.obj)
        return

    def EvalSpline(self, _id, resolution):
        if( _id > (self.splines-1) or self.status != ""): return []
        a_len = 3*resolution
        lib.Model_EvaluateSpline.restype = ctypes.POINTER(c_double*a_len)
        result = lib.Model_EvaluateSpline(self.obj, ctypes.c_int(_id), ctypes.c_int(resolution))
        retlist = [i for i in result.contents]
        dealloc_data(result)
        return retlist
    
    def EvalSurface(self, _id, resolution):
        if( _id > (self.surfaces - 1) or self.status != ""): return []
        a_len = 6*resolution*resolution
        lib.Model_EvaluateSurface.restype = ctypes.POINTER(c_double*a_len)
        result = lib.Model_EvaluateSurface(self.obj, ctypes.c_int(_id), ctypes.c_int(resolution))
        retlist = [i for i in result.contents]
        dealloc_data(result)
        return retlist

    def __exit__(self):
        lib.Model_delete(self.obj)
        return

def dealloc_data(block):
    lib.Data_delete(block)
    return

def print_mem(string):
    import psutil, os
    process = psutil.Process(os.getpid())
    print string + str(process.get_memory_info()[0])
    return

def Eval(npts, data_string):
    m = Model(str(data_string))
    if(m.status != ""): return m.status
    result = "\n"
    result += str(m.splines) + " " + str(m.surfaces) + "\n"
    for i in range(m.splines):
        spline_data = m.EvalSpline(i, npts)
        for j in range(npts):
            result += str(spline_data[j*3]) + " " + str(spline_data[j*3+1]) + " " + str(spline_data[j*3+2]) + "\n"
    l = npts*npts
    for i in range(m.surfaces):
        surf_data = m.EvalSurface(i, npts)
        for j in range(l):
            result += str(surf_data[j*6]) + " " + str(surf_data[j*6 + 1]) + " " + \
                    str(surf_data[j*6+2]) + " " + str(surf_data[j*6 + 3]) + " " + \
                    str(surf_data[j*6 + 4]) + " " + str(surf_data[j*6 + 5])
            if(i < m.surfaces - 1 or j < l-1): result += "\n"
    return result

