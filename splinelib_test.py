import os
from bottle import route, run, template, get, post, static_file, request
import splinelib

@route('/static/<filename>')
def send_static(filename):
    return static_file(filename, root='/home/schmell/website/static/')

@route('/')
def mainpage():
    return template('./template.html', title="Test title")

@route('/send/', method='POST')
def serve_ajax():
    result =  splinelib.Eval(int(request.json['npts']), str(request.json['data']))
    lines = result.splitlines()
    return result

run(host='localhost', port=8080, debug=True)
#run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
